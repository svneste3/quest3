<?php
//Задание 1
for ($i = 0; $i < 100; $i++) {
    $orders[$i] = [
        'price' => rand(100, 5000),
        'date' => mt_rand(14, 24)
    ];
};
foreach ($orders as $order) {
    $profit += $order['price'];
}
foreach ($orders as $key => $date) {
    if ($date['date'] == 18 || $date['date'] == 19) {
        $lossOrdersInWeekends += $date['price'];
        $countOrders += 1;
    }
}
$totalOrders = count($orders);
echo "Заказов - "  . $totalOrders . " на сумму - " . $profit . ". Из них профукано - " . $countOrders . " заказов на сумму: " . $lossOrdersInWeekends . "<br>";
//Задание 2
$couriersCount = 3;
foreach ($orders as $key => $value) {
    if ($value["date"] != 18 && $value["date"] != 19) {
        $ordersByDate[$value['date']][] = $value;
    }
}
$max = 0;
foreach ($ordersByDate as $dates => $pr) {
    echo "Количество заказов " . $dates . " числа - " .  count($pr) . "<br>";
    echo "Необходимое количество дополнительных курьеров " . $dates . " числа - " . (count($pr) - $couriersCount) . "<br>";
    if (count($pr) > $max) {
        $max = count($pr);
    }
}
echo "Для того чтобы доставить все заказы необходимо минимальное количество куьеров: " . $max . "<br>";
//Задание 3
foreach ($ordersByDate as $dates => $pr) {
    if (count($pr) >= 3) {
        $lossOrdersBecauseFew = count($pr) - $couriersCount;
        $lossOrdersBecauseFewCouriers += $lossOrdersBecauseFew;
    }
}
echo "Заказов профукано из-за недостатка курьеров: " . $lossOrdersBecauseFewCouriers . "<br>";
//Задание 4
function my_sort($a, $b) {
    return $b['price'] <=> $a['price'] ;
}
foreach ($ordersByDate as $dat => $val) {
    usort($ordersByDate[$dat], 'my_sort');
    for ($i = 0; $i < $couriersCount; $i++) {
        $lossBecauseFewCouriers += $val[$i]['price'];
        // $lossBecauseFewCouriers += $ordersByDate[$dat][$i]['price'];
    }
}
echo "<br>";
echo $lossBecauseFewCouriers . "<br>";
$sumLoss = ($profit - $lossBecauseFewCouriers);
echo "Заказов профукано из-за недостатка курьеров на сумму: " . $sumLoss . "<br>";
//Задание 5
echo "Заказов - " . $totalOrders . " на сумму: " . $profit . ". Из них профукано в выходные " . $countOrders . " заказов на сумму: " . $lossOrdersInWeekends . " А также профукано из-за недостатка курьеров " . $lossOrdersBecauseFewCouriers . " на сумму: " . $sumLoss . "<br>";
$total = $totalOrders - $countOrders  - $lossOrdersBecauseFewCouriers;
$totalMoney = $profit - $lossOrdersInWeekends - $sumLoss;
echo "Итого заказов доставлено " . $total . " из " . $totalOrders . ', денег заработано  ' . $totalMoney . " из " . $profit;
