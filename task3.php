<?php
//Задание 1
$animals = [
    "mouse" => [
        "name" => "мышь",
        "characteristics" => "это животное грызун"
        ],
    "horse" => [
        "name" => "конь",
        "characteristics" => "скачет по полям"
        ],
    "pigi" => [
        "name" => "хрюшка",
        "characteristics" => "кушает желуди"
        ]
];
//Задание 2
$animals["mouse"]["legs"] = "4 лапы";
$animals["mouse"]["tail"] = "лысый";
$animals["horse"]["legs"] = "4 ноги";
$animals["horse"]["tail"] = "пушистый";
$animals["pigi"]["legs"] = "4 ноги";
$animals["pigi"]["tail"] = "крючком";

//Задание 3
$animals["mouse"]["tail"] = [
  "tail" => "лысый",
  "lengt" => "10см"
];
$animals["horse"]["tail"] = [
  "tail" => "пушистый",
  "lengt" => "50см"
];
$animals["pigi"]["tail"] = [
  "tail" => "крючком",
  "lengt" => "4см"
];
print_r($animals);
echo "<br>";
//Задание 4
$buildings = [
  "living" => [
    "floor" => 2,
    "color" => "blue"
  ],
  "stall" => [
    "floor" => 1,
    "color" => "green"
  ],
  "hut" => [
    "floor" => 1,
    "color" => "red"
  ],
  "garage" => [
    "floor" => 5,
    "color" => "black"
  ]
];
//Задание 5
$animals["mouse"]["house"] = "living";
$animals["horse"]["house"] = "stall";
$animals["pigi"]["house"] = "hut";
// print_r($animals);

//Задание 6
$farm = [
    "animals" => $animals,
    "buildings" => $buildings
];
//Задание 7
echo $farm["animals"]["pigi"]["legs"] . "<br>";
echo $farm["buildings"]["hut"]["color"] . "<br>";
$animal = "mouse";
echo $animals[$animal]["characteristics"] . "<br>";
echo $animals[$animal]["house"] . "<br>";
echo $farm["buildings"][$farm["animals"]["horse"]["house"]]["floor"];
