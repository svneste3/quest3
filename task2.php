<?php
//Задание 1
$matrix = [
  [1,2,3],
  [4,5,6],
  [7,8,9]
];
//Задание 2
echo $matrix[1][1] . "<br>";
//Задание 3
echo $matrix[0][2] . "<br>";
//Задание 4
$sum = $matrix[1][0] + $matrix[2][0] + $matrix[2][1];
echo $sum . "<br>";
//Задание 5
$users = [
  [
    "name" => "Никита",
    "age" => "29 лет",
    "profession" => "плотник"
  ],
  [
    "name" => "Вася",
    "age" => "13 лет",
    "profession" => "футболист"
  ],
  [
    "name" => "Николай Николаевич",
    "age" => "77 лет",
    "profession" => "профессиональный игрок в DotA"
  ]
];
echo $users[1]["age"] . "<br>";
//Задание 6
$users[] = [
  "name" => "Виталий",
  "age" => "30 лет",
  "profession" => "программист"
];
echo $users[3]["name"] . "<br>";
//Задание 7
$mysteriousUser = [
  "name" => $users[2]["name"],
  "age" => $users[0]["age"],
  "profession" => $users[1]["profession"]
];
print_r($mysteriousUser);
