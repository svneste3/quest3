<?php
//Задание 1
$errorCode = rand(1, 3);
    switch ($errorCode) {
        case 1 :
            echo "Что-то пошло не так";
            break;
        case 2 :
            echo "Что-то пошло так как надо, но мы этого не ждали";
            break;
        case 3 :
            echo "Я в домике";
            break;
    }
//Задание 2
$myNumber = rand(0, 10);
    switch ($myNumber) {
        case 2 :
        case 4 :
        case 6 :
        case 8 :
            echo "Четный. Ты не с нами!";
            break;
        default:
            echo "Добро пожаловать!";
            break;
    }
//Задание 3
$foods = ['Клубника', 'Арбуз', 'Кабачок', 'Патиссон', 'Картошка', 'Яблоко', 'Апельсин', 'Банан', 'Лягушачие лапки'];
$foodItem = rand(0, 8);
     switch ($foods[$foodItem]) {
         case 'Клубника':
         case 'Арбуз':
            $view = 'Ягода';
             break;
         case 'Кабачок':
         case 'Патиссон':
         case 'Картошка':
             $view = 'Овощь';
             break;
         case 'Яблоко':
         case 'Апельсин':
         case 'Банан':
             $view = 'Фрукт';
             break;
         case 'Лягушачие лапки':
             $view = 'Что-то не вегетарианское';
             break;
  }
echo "Выбранный продукт {$foods[$foodItem]} - это {$view }";
