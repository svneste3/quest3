<?php
//Задание 1
$boringToys = [];
for ($i = 0; $i < rand(1, 10); $i++) {
    $boringToys[] = [
        'name' => "Игрушка{$i}",
        'price' => rand(100, 1000)
    ];
};
print_r($boringToys);
echo "<br>";
//Задание 2
$cars = [
    [
        "name" => "Мерседес",
        "price" => 10000
    ],
    [
        "name" => "BWM",
        "price" => 9999
    ],
    [
        "name" => "Автобус",
        "price" => 20000
    ]
];
//Задание 3
foreach ($cars as $key => $value) {
    $sum += $value["price"];
}
echo $sum . "<br>";
//Задание 4
$colors = ['blue', 'red', 'white', 'yelow', 'green', 'gray', 'brown'];
foreach ($cars as $key => $value) {
    for ($i = 0; $i < 3; $i++) {
        $cars[$key]['colors'][] = [
            'color' => $colors[rand(0, count($colors))],
            'addPrice' => rand(0, 100)
        ];
    }
}
print_r($cars);
echo "<br>";
//Задание 5
foreach ($cars as $key => $car) {
    foreach ($cars[$key]['colors'] as $car => $color) {
        $totalPrice = $cars[$key]['price'] + $color['addPrice'];
        echo "Автомобиль -" . $cars[$key]['name'] . " цвета -" . $color['color'] . " всего за - " . $totalPrice . "<br>";
    }
}
